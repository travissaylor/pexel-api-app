import React, {Component} from 'react';
import './components.css'

//Require Wrapper Library
const PexelsAPI = require('pexels-api-wrapper');

//Create Client instance by passing in API key
const pexelsClient = new PexelsAPI("563492ad6f91700001000001b05588ba2782467d939d0410d9713182");

export default class Column extends Component {
  constructor(props) {
    super(props);
    this.state = {
      photos: [],
      loading: true,
      text: this.props.category,
      category: this.props.category,
      photoNumber: 4
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleTextChange = this.handleTextChange.bind(this);
    this.handleLoadMore = this.handleLoadMore.bind(this);
  }

  componentDidMount(props) {
    pexelsClient.search(this.state.category, 4, 1)
      .then(function(result) {

        this.setState({
          photos: result.photos,
          loading: false
        });
      }.bind(this));
  }


  handleSubmit(event) {
    pexelsClient.search(this.state.text, 4, 1)
      .then(function(result) {

        this.setState({
          photos: result.photos,
          loading: false,
          category: this.text,
          photoNumber: 4
        });
      }.bind(this));
  }

  handleTextChange(event) {
    this.setState({
      text: event.target.value
    });
  }

  handleLoadMore(event) {
    pexelsClient.search(this.state.text, this.state.photoNumber + 4, 1)
      .then(function(result) {

        this.setState({
          photos: result.photos,
          loading: false,
          photoNumber: this.state.photoNumber + 4
        });
      }.bind(this));
  }


  render() {
    const photoItems = this.state.photos.map((res) =>
    <div className="imgWrapper">
      <img src={res.src.medium} alt={res.id}/>
    </div>
    );

    return (
      <div className="column">
        <h1>{this.state.text}</h1>
        <form>
          <label>
            Change the image category: 
            <input type="text" value={this.state.text} onChange={this.handleTextChange}/>
          </label>
          <button type="button" onClick={this.handleSubmit}>Change Category</button>
          {photoItems}
          <button type="button" onClick={this.handleLoadMore}>Load More</button>
        </form>

      </div>
    );
  }
}
