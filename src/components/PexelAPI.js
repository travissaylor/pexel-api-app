import React, { Component } from 'react';
import Column from './Column'
import './components.css'



export default class PexelAPI extends Component {
  render() {
    return (
      <div className="row">
        <Column category="food" />
        <Column category="cats" />
        <Column category="milk" />
        <Column category="christmas" />
      </div>
    );
  }
}
