import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import PexelAPI from './components/PexelAPI';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header>
          <h1>Travis Pexel API App</h1>
        </header>
        <PexelAPI />
      </div>
    );
  }
}

export default App;
